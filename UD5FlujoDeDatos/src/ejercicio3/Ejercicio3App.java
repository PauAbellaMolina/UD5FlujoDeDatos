package ejercicio3;
import javax.swing.JOptionPane;

public class Ejercicio3App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String nombre = JOptionPane.showInputDialog(null, "Introduce tu nombre:");
		
		//Mostramos el mensaje en una ventana
		JOptionPane.showMessageDialog(null, "Bienvenido " + nombre);
	}
}
