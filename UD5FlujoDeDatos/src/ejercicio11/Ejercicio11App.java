package ejercicio11;
import javax.swing.JOptionPane;

public class Ejercicio11App {
	public static void main(String[] args) {
		//Pedimos el dia de la semana al usuario
		String diaSemanaInput = JOptionPane.showInputDialog(null, "Introduce un dia de la semana:");
		
		//Switch que dependiendo del dia de la semana introducido, muestre una ventana con un mensaje u otro
		switch (diaSemanaInput) {
			case "Lunes":
				JOptionPane.showMessageDialog(null, "Hoy " + diaSemanaInput + ", es dia laboral. A trabajar!");
				break;
			case "Martes":
				JOptionPane.showMessageDialog(null, "Hoy " + diaSemanaInput + ", es dia laboral. A trabajar!");
				break;
			case "Miercoles":
				JOptionPane.showMessageDialog(null, "Hoy " + diaSemanaInput + ", es dia laboral. A trabajar!");
				break;
			case "Jueves":
				JOptionPane.showMessageDialog(null, "Hoy " + diaSemanaInput + ", es dia laboral. A trabajar!");
				break;
			case "Viernes":
				JOptionPane.showMessageDialog(null, "Hoy " + diaSemanaInput + ", es dia laboral. A trabajar!");
				break;
			case "Sabado":
				JOptionPane.showMessageDialog(null, "Hoy " + diaSemanaInput + ", es dia libre. A descansar!");
				break;
			case "Domingo":
				JOptionPane.showMessageDialog(null, "Hoy " + diaSemanaInput + ", es dia libre. A descansar!");
				break;
		}
	}
}