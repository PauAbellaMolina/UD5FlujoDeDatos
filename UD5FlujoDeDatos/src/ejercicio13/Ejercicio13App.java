package ejercicio13;
import javax.swing.JOptionPane;

public class Ejercicio13App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String primerOperandoInput = JOptionPane.showInputDialog(null, "Introduce el primer operando");
		String segundoOperandoInput = JOptionPane.showInputDialog(null, "Introduce el segundo operando");
		int primerOperandoInt = Integer.parseInt(primerOperandoInput);
		int segundoOperandoInt = Integer.parseInt(segundoOperandoInput);
		
		String signoAritmeticoInput = JOptionPane.showInputDialog(null, "Introduce un signo aritmetico");
		
		double resultado;
		
		//Switch que dependiendo del signo introducido, haga el calculo correspondiente y muestre el resultado en una ventana
		switch (signoAritmeticoInput) {
			case "+":
				resultado = primerOperandoInt+segundoOperandoInt;
				JOptionPane.showMessageDialog(null, primerOperandoInt + " + " + segundoOperandoInt + " = " + resultado);
				break;
			case "-":
				resultado = primerOperandoInt-segundoOperandoInt;
				JOptionPane.showMessageDialog(null, primerOperandoInt + " - " + segundoOperandoInt + " = " + resultado);
				break;
			case "*":
				resultado = primerOperandoInt*segundoOperandoInt;
				JOptionPane.showMessageDialog(null, primerOperandoInt + " * " + segundoOperandoInt + " = " + resultado);
				break;
			case "/":
				resultado = primerOperandoInt/segundoOperandoInt;
				JOptionPane.showMessageDialog(null, primerOperandoInt + " / " + segundoOperandoInt + " = " + resultado);
				break;
			case "^":
				resultado = Math.pow(primerOperandoInt, segundoOperandoInt);
				JOptionPane.showMessageDialog(null, primerOperandoInt + " ^ " + segundoOperandoInt + " = " + resultado);
				break;
			case "%":
				resultado = primerOperandoInt%segundoOperandoInt;
				JOptionPane.showMessageDialog(null, primerOperandoInt + " % " + segundoOperandoInt + " = " + resultado);
				break;
		}
	}
}