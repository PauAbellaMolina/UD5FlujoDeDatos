package ejercicio12;
import javax.swing.JOptionPane;

public class Ejercicio12App {
	public static void main(String[] args) {
		//Definimos variables
		String contrasena = "pass123";
		String contrasenaInput = "";
		
		int intentos = 0;
		
		//Bucle do while que pide al usuario la contraseņa y se repite hasta que este la acierte o se alcancen los 3 intentos
		do {
			contrasenaInput = JOptionPane.showInputDialog(null, "Introduce la contrasena: [Intentos restantes " + intentos + "/3]");
			intentos++;
		} while (intentos < 3 && !contrasenaInput.equals(contrasena));
		
		//Dependiendo de si el usuario ha acertado o no la contraseņa, mostramos un mensaje u otro en una ventana
		if (contrasenaInput.equals(contrasena)) {
			JOptionPane.showMessageDialog(null, "Enhorabuena");
		} else {
			JOptionPane.showMessageDialog(null, "Contrasena incorrecta tras 3 intentos");
		}
	}
}