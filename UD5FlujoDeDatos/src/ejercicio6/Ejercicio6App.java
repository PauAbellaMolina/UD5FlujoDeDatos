package ejercicio6;
import javax.swing.JOptionPane;

public class Ejercicio6App {
	public static void main(String[] args) {
		//Pedimos variables al usuario i definimos variables
		final int IVA = 21;
		
		String precioInput = JOptionPane.showInputDialog(null, "Introduce el precio de un producto:");
		double precioDouble = Double.parseDouble(precioInput);
		
		//Calculamos el precio final
		double precioFinal = precioDouble*IVA/100 + precioDouble;
		
		//Mostramos el resultado en una ventana
		JOptionPane.showMessageDialog(null, "Precio final = " + precioFinal);
	}
}