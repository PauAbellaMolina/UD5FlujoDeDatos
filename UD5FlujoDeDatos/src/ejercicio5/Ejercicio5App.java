package ejercicio5;
import javax.swing.JOptionPane;

public class Ejercicio5App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
		String numeroInput = JOptionPane.showInputDialog(null, "Introduce un numero:");
		double numeroDouble = Double.parseDouble(numeroInput);
		
		String resultado;
		
		//Comprovamos si el numero introducido es divisible entre 2
		if (numeroDouble%2 == 0) {
			resultado = "SI";
		} else {
			resultado = "NO";
		}
		
		//Mostramos el resultado en una ventana
		JOptionPane.showMessageDialog(null, "El numero introducido (" + numeroInput + "), " + resultado + " es divisible entre 2");
	}
}