package ejercicio1;
public class Ejercicio1App {
	public static void main(String[] args) {
		//Definimos variables
		int a = 5;
		int b = 10;
		
		//Comprobamos si es mas grande, mas peque�o o si no es ninguna de las anteriores, es decir, igual
		if (a > b) {
			System.out.println("A �s mas grande que B");
		} else if (a < b) {
			System.out.println("B �s mas grande que A");
		} else {
			System.out.println("A y B son iguales");
		}
	}
}