package ejercicio7;
public class Ejercicio7App {
	public static void main(String[] args) {
		//Defininmos una variable contador
		int cont = 0;
		
		//Bucle while que mientras el contador sea menos de 100, printee el valor de este
		while (cont < 100) {
			cont++;
			System.out.println(cont);
		}
	}
}