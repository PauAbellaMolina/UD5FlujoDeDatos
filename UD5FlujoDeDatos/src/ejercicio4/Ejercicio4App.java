package ejercicio4;
import javax.swing.JOptionPane;

public class Ejercicio4App {
	public static void main(String[] args) {
		//Pedimos variables al usuario
        String radioInput = JOptionPane.showInputDialog(null, "Introduce el radio del circulo:");
        double radioDouble = Double.parseDouble(radioInput);
        
        //Calculamos el area
        double area = Math.PI*Math.pow(radioDouble, 2);
        
        //Mostramos el resultado en una ventana
        JOptionPane.showMessageDialog(null, "Area del circulo: " + area);
    }
}