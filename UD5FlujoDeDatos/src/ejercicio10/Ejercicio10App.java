package ejercicio10;
import javax.swing.JOptionPane;

public class Ejercicio10App {
	public static void main(String[] args) {
		//Pedimos variables al usuario i definimos variables
		String numeroVentasInput = JOptionPane.showInputDialog(null, "Introduce un numero de ventas:");
		int numeroVentasInt = Integer.parseInt(numeroVentasInput);
		
		double sumaVentas = 0;
		
		//Bucle for que mientras el contador sea menos que el numero de ventas intrducido, pida el valor de cada venta y lo sume a la variable definida
		for (int cont = 0; cont < numeroVentasInt; cont++) {
			String valorVentasInput = JOptionPane.showInputDialog(null, "Introduce el valor de la venta:");
			double valorVentasInt = Double.parseDouble(valorVentasInput);
			
			sumaVentas += valorVentasInt;
		}
		
		//Mostramos la variable de la suma del valor de ventas en una ventana emergente
		JOptionPane.showMessageDialog(null, "La suma total de " + numeroVentasInput + " ventas �s: " + sumaVentas);
	}
}