package ejercicio9;
public class Ejercicio9App {
	public static void main(String[] args) {
		//Bucle for que mientras el contador sea menos de 100, printee el valor de este solo si es divisible entre 2 o 3
		for (int cont = 1; cont <= 100; cont++) {
			if (cont%2 == 0) {
				System.out.println(cont);
			} else if (cont%3 == 0) {
				System.out.println(cont);
			}
		}
	}
}